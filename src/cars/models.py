from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models

User = settings.AUTH_USER_MODEL # 'auth.User'

# class Profile(models.Model):
#           user = models.OneToOneField(User)
#           city = models.CharField(max_length=120)
#           image = models.ImageField()

# class CompanyProfile(models.Model):
#           user = models.ForeginKey(User)
#           city = models.CharField(max_lenghth=120)
#           image = models.ImageField()

# user_obj = User.objects.first()
# user_obj.companyprofile_set.all()


class Car(models.Model):
    ## Tak tworzymy relacje OneToOne
    first_owner = models.OneToOneField(User, on_delete=models.CASCADE) # unique
    
    ## Relacja ManyToOne
    #user    = models.ForeignKey(User, on_delete=models.CASCADE)  
    

    ## tak tworzymy relacje ManyToMany
    ##drivers = models.ManyToManyField(User)
    name    = models.CharField(max_length=120)

    def __str__(self): # __unicode__ jest dla python versji 2 
        return self.name

# car_1 = Car.objects.first()
# user_qs car_1.drivers.all() # zwraca wszystkich kierowcow tego samochodu

# User = cfe.__class__ 

# first_user = User.object.first()

# cfe = user.qs.first() # zwraca pierwszego usera z listy user_qs
# cfe.car_set.all() # zwraca wszystkie samochody jakimi ten user sie porusza

# Car.objects.filter(drivers=cfe) # zwraca wszystkie samochody jakie prowadzi user cfe

# Car.object.filter(drivers__in=user_qs) wyrzuci nam 4 samochody jest jest 2 uzytkownikow ktorzy prowadza 3 samochody



## ForeignKey = ManyToOneField() #Many Users can have any car, car can only have
## 1 user 

#User = car_obj.user.__class__

#abc User.objects.all().least()
#user_cars = abc.car_set.all()

##

#user_cars_qs = Car.objects.filter(user=abc)

#class Comment(models.Model):
#    user = models.ForeignKey(User)
#    content = models.CharField(max_legth=120)


#comments = abc.comments_set.all()
#comments_qs =  Comment.objects.filter(user=abc)






